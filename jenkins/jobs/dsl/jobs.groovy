//Folders
def workspaceFolderName = "${WORKSPACE_NAME}"
def projectFolderName = "${PROJECT_NAME}"
def sampleFolderName = projectFolderName + "/SampleFolder"
def sampleFolder = folder(sampleFolderName) { displayName('Folder cretaed by Jenkins') }

//Jobs
def Job1 = freeStyleJob(sampleFolderName + "/FirstJob")
def Job2 = freeStyleJob(sampleFolderName + "/SecondJob")

//Pipeline
def samplepipeline = buildPipelineView(sampleFolderName + "/Sample-Pipeline-Demo")

samplepipeline.with{
	title('Pipeline Demo')
	displayedBuilds(5)
	selectedJob(sampleFolderName + "/FirstJob")
	showPipelineParameters()
	refreshFrequency(5)
}
// Job Configurations
Job1.with{
	scm{
		git{
			remote{
				url('https://kishikaira22@bitbucket.org/kishikaira22/cartridge.git')
				credentials("adop-jenkins-master")
				}
				branch("*/master")
			}
		}
		steps{
		shell('''#!/bin/sh
		ls -lart''')
		}
		publishers{
			downstreamParameterized{
				trigger(sampleFolderName + "/SecondJob"){
				condition("SUCCESS")
				parameters{
					predefinedProp("CUSTOM_WORKSPACE",'$WORKSPACE')
				}
			}
		}
    }
}
Job2.with{
	parameters{
			stringParam("CUSTOM_WORKSPACE","","")
	}
	steps{
	shell('''
	#!/bin/sh
	ls -lart $CUSTOM_WORKSPACE
	''')
	}
}